import React from 'react'
import {
  AppRegistry,
} from 'react-native'

import WebViewClass from "./WebViewClass"

class Calpill extends React.Component {
  render() {
    return (
      <WebViewClass/>
    )
  }
}

AppRegistry.registerComponent('Calpill', () => Calpill);
