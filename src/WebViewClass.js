import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  WebView,
  Dimensions,
} from 'react-native'

const DEFAULT_URL = '10.0.0.1'

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#3b5998",
  },
  webView: {
    backgroundColor: "rgba(255,255,255,0.8)",
    height: screenHeight,
    width: screenWidth,
  }
})

export default class WebViewClass extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={{height: 10}}/>
        <WebView
          automaticallyAdjustContentInsets={false}
          style={styles.webView}
          source={{uri: DEFAULT_URL}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          decelerationRate="normal"
          onShouldStartLoadWithRequest={() => this.onShouldStartLoadWithRequest()}
          startInLoadingState={true}
          scalesPageToFit={true}
        />
      </View>
    );
  }

  onShouldStartLoadWithRequest() {
    return true;
  }
}
