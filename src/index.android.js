import React from 'react'
import {
  AppRegistry,
} from 'react-native'

import WebViewClass from "./WebViewClass"

class TestProject extends React.Component {

  render() {
    return (
      <WebViewClass/>
    );
  }
}

AppRegistry.registerComponent('TestProject', () => TestProject);
